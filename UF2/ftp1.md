# FTP:

En aquesta primera pràctica instal·lareu un servidor FTP a una MV Ubuntu (podeu fer servir la mateixa de SQUID, Apache...).

Heu de fer les següents accions i demostrar-les amb captures:

1.- Instal·lar ProFTPD a la MV. Teniu un tutorial [aqui](https://dungeonofbits.com/instalacion-y-configuracion-del-servidor-ftp-proftpd.html).

![ftp](UF2/imagen/ftpon.png)


con sudo apt update después el comando  = sudo apt install proftpd, instalamos el proftpd.

2.- Fes que qualsevol usuari que entri al teu FTP accedeixi al seu directori home.

![home](UF2/imagen/home.png)

Con el cambio de la configuración en /etc/proftpd/proftpd.conf, todos los usuarios accederán en el directorio home

3.- Canvia el nom del servidor de FTP al teu cognom.

![nome](UF2/imagen/nome1.png)
En /etc/proftpd/proftpd.conf al cambiar el Servername y un reload, ya funcionará bien

![nome2](UF2/imagen/nome2.png)



4.- Personalitza el missatge de benvinguda i el d'error d'accés del teu servidor.

![messa](UF2/imagen/messa.png)
En /etc/proftpd/proftpd.conf al añadir AccessGrantMsg "Link Start"
AccessDenyMsg "404 Error", tenemos configurado.

5.- Accedeix al teu servidor des d'un altre equip i fes:
    
* Un llistat dels fitxers que hi ha.


![llistat](UF2/imagen/llistat.png)
con el comando ls, podemos mirar la lista de archivos


* Puja un fitxer anomenat X.txt (on X és el teu cognom). 

![put](UF2/imagen/put.png)
con el comando put, podemos subir un fichero

* Descarrega el fitxer X.txt del servidor al teu equip.

![get](UF2/imagen/get.png)
con el get, podemos descargalos 

6.- Crea un usuari per a un company que sigui el seu nom (el password també el seu nom) i que provi d'accedir al FTP i pujar un fitxer.

![daniftp](UF2/imagen/uiliamftp.png)


7.- Canvia les directives del servidor perquè els usuaris del grup public accedeixin al directori /home/public (que hauràs de crear) i la resta d'usuaris accedeixi al seu home.

![public](UF2/imagen/public.png)

8.- Canvia el usuari d'un company al grup public.



9.- Comprova que l'usuari anterior accedeix al directori /home/public.

10.- Comprova que tu, amb el teu usuari que no està al grup public accedeixes al teu directori home.

11.- Canvia les directives de login perquè un usuari d'un company no pugui fer login al teu servidor i el teu usuari si pugui fer login.

12.- Prova les directives anteriors.


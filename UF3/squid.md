<img src="https://s24255.pcdn.co/wp-content/uploads/2013/12/squid_logo.png" />

# Pràctica Proxy Squid 1:

Ara que ja saps instal·lar i configurar el proxy Squid [recorda el tutorial que tens aquí](https://dungeonofbits.com/instalacion-de-squid-en-linux.html) podem començar a treballar en el nostre propi proxy.

Per orientar-te tens la documentació de Squid [aquí](https://wiki.squid-cache.org/SquidFaq/SquidAcl#Access_Controls_in_Squid)

Potser hauràs de canviar la configuració de squid.conf per a cada punt, així que serà millor que guardis una copia de seguretat cada vegada amb la configuració funcionant i facis les captures abans de canviar-la.

## 1.- Instal·lació:

	Instal·la Squid (mostra una captura del proxy funcionant i que al terminal es vegi el teu usuari):

![funciona](imagenes/status.png)

## 2.- Configurar accés per IP:

	Mostra captures que demostrin els següents punts:

	*1.- Crea un fitxer anomenat permesos.txt amb les IPs dels equips de dos companys de classe.
	
	*2.- Crea un fitxer anomenat prohibits.txt amb les IPs dels equips de dos companys de classe. 
	
	*3.- Crea una ACL anomenada X_seal_of_aprovement (on X és el teu cognom) amb les IPs del fitxer permesos.txt
	
	*4.- Crea una ACL anomenada X_death_note (on X és el teu cognom) amb les IPs del fitxer prohibits.txt
	
	*5.- Permet les connexions dels equips de la llista X_seal_of_aprovement.
	
	*6.- Prohibeix les connexions dels equips de la llista X_death_note.
	
	*7.- Reinicia el proxy i comprova amb els companys que els equips de la llista permesos tenen accés a Internet mitjançant el teu proxy i la resta d'equips no poden accedir.
	
	*8.- Comprova amb el mestre si ell pot accedir des de el seu ordinador a Internet mitjançant el teu proxy. 
	
	*9.- Explica perquè té accés o no amb les teves paraules i mostrant una captura de squid.conf on es demostri el que dius.

1 - ![permesos](UF3/imagen/permesos.png)

2 - ![prohibits](UF3/imagen/prohibits.png)

3,4,5,6 - ![world](UF3/imagen/permet.png)

7 - ![santi](UF3/imagen/santibloqueado.png)

8 - ![mestre](UF3/imagen/uiliam1.png)

9 - 

## 3.- Configurar dominis permesos i prohibits:

	Mostra captures que demostrin els següents punts:

	*1.-Crea un fitxer anomenat ban.txt amb 10 dominis que no es podran accedir mitjançant el teu proxy.
	
	*2.-Crea una ACL que es digui X_banned_domains (on X és el teu cognom), amb els dominis del fitxer ban.txt.
	
	*3.-Crea una norma que prohibeixi l'accés als dominis de la ACL X_banned_domains als usuaris del proxy.
	
	*4.-Comprova amb dos companys que tinguin accés a connexions al teu proxy que poden accedir a tots els dominis menys els de la llista X_banned_domains.
	
	*5.-Fes la prova amb el mestre.

1 - ![ban](UF3/imagen/ban.png)

2, 3, - ![2](UF3/imagen/2.png)

4 - ![ds](UF3/imagen/friday_uil.png)

5 - ![2](UF3/imagen/p3.png)


## 4.- Configurar patrons de url prohibits:

	Mostra captures que demostrin els següents punts:
	
	*1.-Crea una ACL anomenada X_banned_words que contingui una llista de paraules (mínim 10).

	*2.-Crea una norma que faci que qualsevol url que contingui una paraula de la llista X_banned_words sigui bloquejada pel proxy.
	
	*3.-Demostra-ho amb un company.
	
	*4.-Fes la prova amb el mestre.

1, 2 -  ![ban_words](UF3/imagen/ban_words.png)

3 - ![manga](UF3/imagen/mangalones.png)

4 - ![ubuntu](UF3/imagen/ubuntu.png)

## 5.- Accés exclusiu:

	Mostra captures que demostrin els següents punts:
	
	*1.-Crea una ACL amb la IP del mestre.
	
	*2.-Crea una ACL amb la URL /var/www/html/mestre/index.html on aquest index.html serà el teu CV.

	*3.-Fes que només la IP del mestre pugui accedir a la url.
	
	*4.-Demostra-ho amb un company.
	
	*5.-Demostra-ho amb l'equip local del proxy.
	
	*6.-Demostra-ho amb el mestre.

1, 2, 3 - ![acl](UF3/123.png)

4 - ![dani](UF3/imagen/dani_pajaro.png)

5 - ![proxu](UF3/imagen/proxu.png)

6 - ![proxu](UF3/uiliam.png)

## 6.- Log de Squid:
 
El log del servei Squid es trova al directori: /var/log/squid/. 

* Mostra captures que demostrin els següents punts:
	
* Mostra captura del log indicant clarament els intents de connexió del punt 5 (Connexió rebutjada al equip del company, rebutjada al equip local, aprovada per a l'equip del mestre).

## 7.- Horari de connexió:

Es poden crear horaris dins la configuració de Squid, la forma de  fer-ho és dient-li a Squid els dies de la setmana i l'interval d'hores, per exemple:

```
acl horari_smx_tarda MTWHF 15:00-21:30
```

Aquesta acl és un interval de hores de les 3 de la tarda fins les 9 i mitja del vespre de dilluns a divendres.

Els dies de la setmana a Squid són, de dilluns a diumenge: MTWHFAS.

Les hores van en format de 24 hores: 00:00 a 23:59

* Crea una restricció amb l'horari de les classes de SMX 2T de M07, fent que a aquelles hores només es pugui accedir a gitlab.com, dungeonofbits.com i iespoblenou.org
* Copia aqui les acl i les restriccions.
* Comprova si pots accedir a google.com (NO) i a gitlab.com (SI) i mostra la captura de /var/log/acces.log que ho demostra.
    
## 8.- Resum de configuracions de Squid:

En aquest apartat només has de copiar el contingut de squid.conf per als següents casos:

1. A la teva empresa els usuaris de la xarxa 192.168.1.0/24 només poden accedir a youtube.com de 13:00 a 15:00 de dilluns a dijous (que és l'horari de dinar, perquè els divendres a la tarda no es treballa). Durant la resta de l'horari no es pot accedir a youtube.com però sí a la resta de webs.

2. Has detectat que els teus usuaris accedeixen a pàgines d'apostes i el teu cap no vol que ho puguin fer, l'únic problema és que el teu cap SÍ QUE VOL ACCEDIR a aquestes pàgines. Busca 5 pàgines d'empreses de juguesques online i crea una acl amb elles, deixa que el teu cap a la IP 172.31.84.197 pugui accedir a la llista però que la resta d'usuaris no.  Tota la resta de pàgines web són accesibles.

3. Tens una empresa de productes vegans i vols evitar que els usuaris de la xarxa accedeixin a cap pàgina que parli de productes càrnics. Fes una llista de 5 paraules de productes no aptes per a vegans i prohibeix la búsqueda d'aquestes paraules com a part de dominis als usuaris. Tota la resta de pàgines web són accesibles.

4. La empresa TRINIDAD té dos CEOs, en Bud que es fan del Barça i el Terence, que és fan de l'Espanyol, per aquest motiu a la seva empresa només es pot accedir a la web preferida per Bud, www.fcbarcelona.es, els dilluns, dimecres i divendres de 08:00 a 20:00. Els dimarts i dijous només es pot accedir a www.rcdespanyol.com, l'equip favorit del Terence al mateix marc horari. Els caps de setmana només es pot accedir a la web www.realzaragoza.com durant tot el dia perquè és l'equip preferit del vigilant de seguretat.

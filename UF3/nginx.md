# Práctica con el servidor web nginx:

En ésta práctica instalarás el servidor web Nginx, para ello puedes seguir [este tutorial](https://dungeonofbits.com/instalacion-de-nginx-en-linux.html).

Una vez lo tengas instalado crearás dos sitios virtuales:

1.- APELLIDO.com (Dónde APELLIDO es tu apellido en minúsculas).

2.- AFICIÓN.com (Dónde AFICIÓN es un hobby tuyo o una película/artista/cómic/etc... que te guste en minúsculas).

Cada sitio será accesible desde tu navegador.

3.- Crearás un fichero html llamado *index.html* en cada uno de los directorios de los dos sitios web que deberían estar en directorios distintos dentrro de */var/www/html* explicando un poco la temática de la web. Por ejemplo el de APELLIDO.com podría ser tu curriculum y el de AFICIÓN.com podría ser una página sencilla dedicada a la afición.

4.- En AFICIÓN.com crearás un directorio llamado *public* y dentro de éste un fichero llamado *index.html*, este fichero contendrá algún tipo de información EXTRA de tu afición.

5.- Crearás un fichero llamado *.htpasswd* en el directorio */etc/nginx* que tendrá el siguiente formato:

```
#Fichero de contraseñas:
usuario1:password1
usuario2:password2
```

Con al menos un usuario, puede contener los que quieras.

En este fichero usuario1 será un nombre de usuario y passwrod1 la contraseña de dicho usuario. Para generar esta contraseña deberás usar el comando:

```
openssl passwd contraseña
```

Que te devolverá la contraseña encriptada que corresponde a password1 en el fichero.

Una vez tengas el fichero terminado guárdalo (utiliza contraseñas fáciles para recordarlas...).

6.- El siguiente paso hacer que APELLIDO.com solo sea accesible por contraseña, para ello utilizarás las directivas auth_basic y auth_basic_user_file tal y como se muestra en [la documentación de nginx](https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-http-basic-authentication/).

7.- El siguiente paso hacer que APELLIDO.com solo sea accesible por contraseña, pero el directorio *public* tenga libre acceso (sigue la documentación de nginx).



**Preguntas y capturas:**

1. Captura del fichero de configuración del sitio web APELLIDO.com.
 

![mateu.com](UF3/imagen/mateu.png)

2. Captura del fichero de configuración del sitio web AFICIÓN.com.

![club.com](UF3/imagen/club.png)

3. Captura de las webs de APELLIDO.com y AFICIÓN.com

![mateu.co](UF3/imagen/pma.png)

![club.co](UF3/imagen/pclub.png)

4. Captura de la web public de AFICIÓN.com

![public](UF3/imagen/public.png)

5. ¿Qué es openssl?

Consiste en un robusto paquete de herramientas de administración y bibliotecas relacionadas con la criptografía, que suministran funciones criptográficas a otros paquetes como OpenSSH y navegadores web

6. ¿Qué algoritmo utiliza openssl por defecto?

SSLeay y programado en C y assembler
7. Captura del fichero de configuración del sitio web APELLIDO.com con autenticación.

![auth](UF3/imagen/me.png)

8. Captura del fichero de configuración del sitio web AFICIÓN.com con autenticación.

![auth](UF3/imagen/auth.png)


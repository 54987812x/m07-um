#Instal·lar sarg per a Squid:

En aquesta pràctica has d'instal·lar sarg per a Squid. Tens un [tutorial aquí](https://dungeonofbits.com/instalacion-y-configuracion-de-sarg-para-squid.html).

1. Explica com has instal·lat sarg.

Demana a algun company (o fes servir diferents equips) que es connecti a Internet mitjançant el teu proxy Squid.

2. Mostra una captura de access.log amb les connexions.

3. Mostra una captura de sarg amb les mateixes connexions. 
 

![IIS-logo](https://luigiasir.files.wordpress.com/2017/11/microsoft-iis_1.png?w=500)

En esta práctica activarás el servicio IIS en un equipo con Windows 10 instalado, puede ser una máquina virtual o una máquia real, a tu elección.

[Aquí](https://dungeonofbits.com/activacion-de-internet-information-services-iis.html) tienes un tutorial para activar IIS en el equipo.

1.- Activa IIS en el equipo y documenta el proceso.

1 - Necesitamos entrar en el panel de control 

![Panel](imagenes/panel.png)

2 - Vamos en Programas

![Programas](imagenes/programas.png)

3 - Entramos en la opción activar y desactivar características de Windows.

![acti](imagenes/acti.png)

4 - Activar el Internet Information Services

![inter](imagenes/inter.png)


2.- Crea un grupo de aplicaciones que se llame "APELLIDO_pool", donde APELLIDO es tu apellido.

![apli](imagenes/apli.png)

3.- Cambia el nombre de la página por defecto que viene en IIS por "Smixers_APELLIDO", donde APELLIDO es tu apellido.

![apli](imagenes/mateu1.png)


4.- Haz que la página "Smixers_APELLIDO" pertenezca al Application pool "APELLIDO_pool".

![apli](imagenes/camv.png)


5.- ¿Si se colgase por algún error una página del "Application pool 1" se podría seguir accediendo a una página de otro Application pool?

6.- Limita el acceso al sitio "Smixers_APELLIDO" a 10 conexiones simultáneas y documenta el proceso.

7.- ¿El examen de directorios está habilitado por defecto en IIS?

8.- ¿Qué indica el examen de directorios?

9.- ¿Cual es el directorio por defecto para el sitio de IIS?

10.- Agrega un fichero html llamado "APELLIDO.html" al directorio por defecto de IIS, este documento contendrá tu CV (El que ya hiciste en M08) o uno nuevo en formato HTML. Si lo tienes en formato md puedes utilizar un conversor md a html online.

11.- Cambia las prioridades en "Documento predeterminado" del sitio para que cuando accedemos a "localhost" por el navegador muestre el curriculum del punto anterior en lugar de la página por defecto de IIS.

12.- Agrega un directorio virtual llamado Poblenou que se aloje en una carpeta llamada poblenou dentro del directorio por defecto de IIS.

13.- Crea una página html en la carpeta anterior que se llame index.html y contenga, al menos, tu apellido y la palabra Poblenou en ella.

14.- ¿Qué debes escribir en tu navegador para mostrar el fichero creado en el punto anterior?

15.- Crea una página de error 404 personalizada con texto e imágenes para tu sitio (puedes basarte en las de google u otro sitio web).

16.- Edita la configuración de tu sitio para que cuando se intente acceder a una página que no exista muestre la página que has creado en el punto anterior y documenta el proceso.

17.- Muestra el fichero log de tu sitio "Smixers_APELLIDO".
